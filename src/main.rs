//! A Brainfuck Interpreter, written in Rust.

use std::path::PathBuf;
use std::process;

use structopt::StructOpt;

use bft_interp::BrainfuckVM;
use bft_types::BrainfuckProgram;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "bft",
    about = "A Brainfuck Interpreter, written in Rust",
    author
)]
struct Opt {
    #[structopt(short, long)]
    cells: Option<usize>,

    #[structopt(short, long)]
    extensible: Option<bool>,

    #[structopt(parse(from_os_str))]
    input: PathBuf,
}

fn run_bft(opt: Opt) -> Result<(), Box<dyn std::error::Error>> {
    let mut bf_program = BrainfuckProgram::from_file(opt.input)?;

    bf_program.validate_brackets()?;

    let cells = match opt.cells {
        Some(c) => c,
        None => 0,
    };

    let extensible = match opt.extensible {
        Some(e) => e,
        None => false,
    };

    let mut bf_vm = BrainfuckVM::<u8>::new(cells, extensible, &mut bf_program);

    bf_vm.interpret(&mut std::io::stdin(), &mut std::io::stdout())?;

    Ok(())
}

fn main() {
    let opt = Opt::from_args();

    let result = run_bft(opt);

    if result.is_err() {
        println!("bft: Error: {}", result.err().unwrap());
        process::exit(1);
    }
}
