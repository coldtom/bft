#![deny(missing_docs)]

//! This Crate contains useful basic types for implementing a Brainfuck Interpreter. In particular
//! there are types for the eight Brainfuck instructions as well as a BrainfuckProgram type to hold
//! an entire parsed file.

use std::collections::HashMap;
use std::fmt;
use std::io;
use std::path::{Path, PathBuf};

#[derive(Clone, Copy, Debug, PartialEq)]
/// An enum to describe the Raw Brainfuck Instructions
///
/// Core representation of an instruction. Gives a more meaningful name than e.g. `.` for
/// instructions.
pub enum RawInstruction {
    /// Represents a `<`.
    ///
    /// Interpreted as "Move the pointer left by one."
    MoveLeft,
    /// Represents a `>`.
    ///
    /// Interpreted as "Move the pointer right by one."
    MoveRight,
    /// Represents a `+`.
    ///
    /// Interpreted as "Increment the current location by one."
    Increment,
    /// Represents a `-`.
    ///
    /// Interpreted as "Decrement the current location by one."
    Decrement,
    /// Represents a `,`.
    ///
    /// Interpreted as "Accept a single byte of input from stdin and store it in the current location."
    Input,
    /// Represents a `.`.
    ///
    /// Interpreted as "Output the single byte at the current location."
    Output,
    /// Represents a `[`.
    ///
    /// Interpreted as "If the current location is zero, then move to the instruction after the
    /// matching `]`."
    BeginLoop,
    /// Represents a `]`.
    ///
    /// Interpreted as "If the current location is nonzero, then move to the instruction after the
    /// matching `[`."
    EndLoop,
}

impl RawInstruction {
    /// Method to parse a char into a RawInstruction
    fn from_char(input: char) -> Option<Self> {
        match input {
            '<' => Some(Self::MoveLeft),
            '>' => Some(Self::MoveRight),
            '+' => Some(Self::Increment),
            '-' => Some(Self::Decrement),
            ',' => Some(Self::Input),
            '.' => Some(Self::Output),
            '[' => Some(Self::BeginLoop),
            ']' => Some(Self::EndLoop),
            _ => None,
        }
    }

    /// Method to represent a RawInstruction as a String.
    ///
    /// Used to generate verbose debug output.
    fn as_debug_str(&self) -> &str {
        match self {
            RawInstruction::MoveLeft => "Move pointer left",
            RawInstruction::MoveRight => "Move pointer right",
            RawInstruction::Increment => "Increment current location",
            RawInstruction::Decrement => "Decrement current location",
            RawInstruction::Input => "Input at current location",
            RawInstruction::Output => "Output current location",
            RawInstruction::BeginLoop => "Start looping",
            RawInstruction::EndLoop => "Stop looping",
        }
    }
}

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_debug_str())
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
/// A representation of a loaded instruction.
///
/// Represents the actual loaded instruction, with additional metadata on the position of the
/// instruction in the file. Metadata is only used for debugging.
///
/// ```
/// # use bft_types::Instruction;
/// let instruction = Instruction::from_annotated_char(1,1,'>').unwrap();
/// ```
pub struct Instruction {
    instruction: RawInstruction,
    line: usize,
    column: usize,
}

impl Instruction {
    /// Create a new instruction from the raw components.
    ///
    /// Takes a RawInstruction rather than a char so as to guarantee non-failure.
    fn new(line: usize, column: usize, instruction: RawInstruction) -> Self {
        Self {
            line,
            column,
            instruction,
        }
    }

    /// Create a new instruction from a char annotated with line and column numbers
    ///
    /// The recommended way to create a new instruction. Takes integers for line and column numbers
    /// for debug data, and a char to be parsed.
    ///
    /// ```
    /// # use bft_types::Instruction;
    /// let instruction = Instruction::from_annotated_char(2,5,'+');
    /// ```
    pub fn from_annotated_char(line: usize, column: usize, instruction: char) -> Option<Self> {
        match RawInstruction::from_char(instruction) {
            Some(instruction) => Some(Self::new(line, column, instruction)),
            None => None,
        }
    }

    /// Get the line that this instruction was read from.
    ///
    /// ```
    /// # use bft_types::Instruction;
    /// let instruction = Instruction::from_annotated_char(3,4,'.').unwrap();
    /// assert_eq!(instruction.get_line(), 3);
    /// ```
    pub fn get_line(&self) -> usize {
        self.line
    }

    /// Get the column that this instruction was read from
    ///
    /// ```
    /// # use bft_types::Instruction;
    /// let instruction = Instruction::from_annotated_char(7,6,'-').unwrap();
    /// assert_eq!(instruction.get_column(), 6);
    /// ```
    pub fn get_column(&self) -> usize {
        self.column
    }

    /// Get the instruction type for this instruction.
    ///
    /// ```
    /// # use bft_types::{Instruction, RawInstruction};
    /// let instruction = Instruction::from_annotated_char(64,12, '+').unwrap();
    /// assert_eq!(instruction.command(), RawInstruction::Increment);
    /// ```
    pub fn command(&self) -> RawInstruction {
        self.instruction
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.instruction)
    }
}

#[derive(Debug, PartialEq)]
/// Represents an entire loaded program
///
/// Contains a vector of `Instruction`s along with the name of the file loaded.
///
/// ```
/// # use bft_types::BrainfuckProgram;
/// let bf_program = BrainfuckProgram::from_file("foo.txt");
/// ```
pub struct BrainfuckProgram {
    filename: PathBuf,
    instructions: Vec<Instruction>,
    bracket_pairs: HashMap<usize, usize>,
}

impl BrainfuckProgram {
    /// Creates a new BrainfuckProgram
    ///
    /// Parses a string-like into a meaningful brainfuck program, and adds a filename as provenance
    /// metadata.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    ///
    /// BrainfuckProgram::new("foo.bf", "[]+-.,<>>");
    /// ```
    pub fn new<P: AsRef<Path>, S: AsRef<str>>(path: P, content: S) -> Self {
        let instructions = content
            .as_ref()
            .lines()
            .enumerate()
            .flat_map(|(n, l)| {
                l.chars()
                    .enumerate()
                    .filter_map(move |(m, c)| Instruction::from_annotated_char(n + 1, m + 1, c))
            })
            .collect();
        let filename = path.as_ref().to_path_buf();

        Self {
            filename,
            instructions,
            bracket_pairs: HashMap::new(),
        }
    }

    /// Reads a Brainfuck Program from a file
    ///
    /// Takes a path-like and reads the contents into a meaningful representation of the contained
    /// brainfuck program.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// let bf_program = BrainfuckProgram::from_file("helloworld.bf");
    /// ```
    pub fn from_file<P: AsRef<Path>>(path: P) -> io::Result<BrainfuckProgram> {
        let content = std::fs::read_to_string(&path)?;

        Ok(Self::new(path, content))
    }

    /// Validate that square brackets are all matched.
    ///
    /// Returns Ok() if all brackets are matched, or an Err(BfError) containing a helpful error
    /// message informing the user where they have an unmatched bracket.
    ///
    /// Also populates the bracket_pairs hashmap, which stores the positions of matching
    /// brackets.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[]");
    ///
    /// assert_eq!(Ok(()), bf_program.validate_brackets());
    /// ```
    pub fn validate_brackets(&mut self) -> Result<(), BfError> {
        if self.bracket_pairs != HashMap::new() {
            return Ok(());
        }
        let mut open_brackets: usize = 0;
        let mut open: Vec<(usize, Instruction)> = Vec::new();

        for (pos, instruction) in self.instructions.clone().into_iter().enumerate() {
            if instruction.command() == RawInstruction::BeginLoop {
                open_brackets += 1;
                open.push((pos, instruction));
            }
            if instruction.command() == RawInstruction::EndLoop {
                if open_brackets == 0 {
                    return Err(BfError::new(instruction, "Unmatched ']'"));
                } else {
                    let (last_open_pos, _) = open.pop().unwrap();
                    self.bracket_pairs.insert(last_open_pos, pos);
                    self.bracket_pairs.insert(pos, last_open_pos);
                    open_brackets -= 1;
                };
            }
        }

        match open_brackets {
            0 => Ok(()),
            _ => Err(BfError::new(open.last().unwrap().1, "Unmatched '['")),
        }
    }

    /// Get the position of the matching bracket to the one entered
    ///
    /// It is the user's responsibility to not feed this a position outside of the vector, not
    /// doing so will panic!
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[]");
    ///
    /// assert!(bf_program.validate_brackets().is_ok());
    /// assert_eq!(bf_program.match_bracket(0).unwrap(), 1);
    /// assert_eq!(bf_program.match_bracket(1).unwrap(), 0);
    /// ```
    pub fn match_bracket(&mut self, position: usize) -> Result<usize, BfError> {
        self.validate_brackets()?;
        match self.bracket_pairs.get(&position) {
            Some(n) => Ok(*n),
            None => {
                let inst = *self.instruction_at(position).unwrap();
                match inst.command() {
                    RawInstruction::BeginLoop => Err(BfError::new(inst, "Unmatched ']'")),
                    RawInstruction::EndLoop => Err(BfError::new(inst, "Unmatched '['")),
                    _ => Err(BfError::new(inst, "Attempted to match non-bracket char!")),
                }
            }
        }
    }

    /// Return the length of the underlying vector of instructions
    ///
    /// ```
    /// # use bft_types::{BrainfuckProgram, Instruction, RawInstruction};
    ///
    /// let bf_program = BrainfuckProgram::new("foo", "[.+");
    /// assert_eq!(bf_program.len(), 3);
    /// ```
    pub fn len(&self) -> usize {
        self.instructions.len()
    }

    /// Check whether the underlying vector is empty
    ///
    /// ```
    /// # use bft_types::{BrainfuckProgram, Instruction, RawInstruction};
    ///
    /// let bf_program = BrainfuckProgram::new("foo", "");
    /// assert!(bf_program.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.instructions.is_empty()
    }

    /// Get a borrow of the instruction at position `position`.
    ///
    /// Allows access to the underlying vector of instructions
    ///
    /// ```
    /// # use bft_types::{BrainfuckProgram, Instruction, RawInstruction};
    ///
    /// let bf_program = BrainfuckProgram::new("foo", "[");
    ///
    /// assert_eq!(bf_program.instruction_at(0),
    /// Some(&Instruction::from_annotated_char(1,1,'[').unwrap()));
    /// ```
    pub fn instruction_at(&self, position: usize) -> Option<&Instruction> {
        self.instructions.get(position)
    }

    /// Formats a single instruction into a nice debug format
    ///
    /// Given an instruction, formats using the filename into something of the form
    /// ```txt
    /// [foo.txt:3:4] Move pointer left
    /// ```
    fn fmt_line(filename: PathBuf, instruction: Instruction) -> String {
        let filename = filename.to_string_lossy();
        format!(
            "[{}:{}:{}] {}.\n",
            filename,
            instruction.get_line(),
            instruction.get_column(),
            instruction
        )
    }
}

impl fmt::Display for BrainfuckProgram {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let output: String = self
            .instructions
            .clone()
            .into_iter()
            .map(|i| BrainfuckProgram::fmt_line(self.filename.clone(), i))
            .collect();
        write!(f, "{}", output)
    }
}

#[derive(Debug, PartialEq)]
/// Error type for Brainfuck errors
///
/// Holds an error message and the instruction which caused the naughtiness.
pub struct BfError {
    instruction: Instruction,
    message: String,
}

impl BfError {
    /// Create a new BfError
    ///
    /// Stores a message to be passed to output
    pub fn new(instruction: Instruction, message: &str) -> Self {
        Self {
            instruction,
            message: message.to_string(),
        }
    }
}

impl fmt::Display for BfError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}:{}: {}",
            self.instruction.get_line(),
            self.instruction.get_column(),
            self.message
        )
    }
}

impl std::error::Error for BfError {}

#[cfg(test)]
mod bft_types_tests {
    use std::path::PathBuf;

    use super::*;

    fn new_bf_program_from_content(filename: &str, content: &str) -> BrainfuckProgram {
        let filename: PathBuf = PathBuf::from(filename);
        BrainfuckProgram::new(&filename, content)
    }

    #[test]
    fn test_raw_instruction_from_char() {
        assert_eq!(
            RawInstruction::from_char('<'),
            Some(RawInstruction::MoveLeft)
        );
        assert_eq!(
            RawInstruction::from_char('>'),
            Some(RawInstruction::MoveRight)
        );
        assert_eq!(
            RawInstruction::from_char('+'),
            Some(RawInstruction::Increment)
        );
        assert_eq!(
            RawInstruction::from_char('-'),
            Some(RawInstruction::Decrement)
        );
        assert_eq!(RawInstruction::from_char(','), Some(RawInstruction::Input));
        assert_eq!(RawInstruction::from_char('.'), Some(RawInstruction::Output));
        assert_eq!(
            RawInstruction::from_char('['),
            Some(RawInstruction::BeginLoop)
        );
        assert_eq!(
            RawInstruction::from_char(']'),
            Some(RawInstruction::EndLoop)
        );

        assert_eq!(RawInstruction::from_char('c'), None);
    }

    #[test]
    fn test_annotated_instruction_from_char() {
        assert_eq!(
            Instruction::from_annotated_char(10, 4, '+'),
            Some(Instruction::new(10, 4, RawInstruction::Increment))
        );

        assert_eq!(
            Instruction::from_annotated_char(6, 2, '['),
            Some(Instruction::new(6, 2, RawInstruction::BeginLoop))
        );

        assert_eq!(Instruction::from_annotated_char(10, 4, 'f'), None);
    }

    #[test]
    fn test_brainfuck_program_new() {
        let filename = PathBuf::from("foo.bf");
        let program: BrainfuckProgram = new_bf_program_from_content("foo.bf", "<>+\n-,\n.[\n]");

        let expected: Vec<Instruction> = vec![
            Instruction::new(1, 1, RawInstruction::MoveLeft),
            Instruction::new(1, 2, RawInstruction::MoveRight),
            Instruction::new(1, 3, RawInstruction::Increment),
            Instruction::new(2, 1, RawInstruction::Decrement),
            Instruction::new(2, 2, RawInstruction::Input),
            Instruction::new(3, 1, RawInstruction::Output),
            Instruction::new(3, 2, RawInstruction::BeginLoop),
            Instruction::new(4, 1, RawInstruction::EndLoop),
        ];

        assert_eq!(
            program,
            BrainfuckProgram {
                filename,
                instructions: expected,
                bracket_pairs: HashMap::new(),
            }
        );
    }

    #[test]
    fn test_brainfuck_program_validate_brackets() {
        let mut bf_program = new_bf_program_from_content("match", "[.,.++-]");
        assert!(bf_program.validate_brackets().is_ok());

        let mut bf_program = new_bf_program_from_content("too_many_close", "[.]-]");
        assert!(bf_program.validate_brackets().is_err());

        let mut bf_program =
            new_bf_program_from_content("too_many_open", "[[[foobar ..,+-]++,.<>]");
        assert!(bf_program.validate_brackets().is_err());
    }

    #[test]
    fn test_brainfuck_program_match_bracket() {
        let mut bf_program = new_bf_program_from_content("foo", "[[[]]]");
        assert!(bf_program.validate_brackets().is_ok());
        assert_eq!(bf_program.match_bracket(0).unwrap(), 5);
        assert_eq!(bf_program.match_bracket(1).unwrap(), 4);
        assert_eq!(bf_program.match_bracket(2).unwrap(), 3);
        assert_eq!(bf_program.match_bracket(3).unwrap(), 2);
        assert_eq!(bf_program.match_bracket(4).unwrap(), 1);
        assert_eq!(bf_program.match_bracket(5).unwrap(), 0);
    }

    #[test]
    fn test_brainfuck_program_instruction_at() {
        let bf_program = new_bf_program_from_content("foo", "[,");

        assert_eq!(
            Some(&Instruction::new(1, 1, RawInstruction::BeginLoop)),
            bf_program.instruction_at(0)
        );
        assert_eq!(None, bf_program.instruction_at(4));
    }

    #[test]
    fn test_brainfuck_program_len() {
        let bf_program = new_bf_program_from_content("foo", "");
        assert!(bf_program.is_empty());

        let bf_program = new_bf_program_from_content("foo", "ab+-cd");
        assert_eq!(bf_program.len(), 2);
    }
}
