#![deny(missing_docs)]

//! A Brainfuck Interpreter Library

use std::fmt::{Debug, Display};
use std::io::{Read, Write};

use bft_types::{BrainfuckProgram, Instruction, RawInstruction};

#[derive(Debug)]
/// An enum to represent error kinds for the Brainfuck Virtual Machine
pub enum BfVMError {
    /// Error for when the head leaves the tape, either by going before the first position or by
    /// falling off the end in the case the VM can grow.
    EndOfTape(Instruction),

    /// Error for errors when performing I/O. Wraps a std::io::Error
    IOError(std::io::Error, Instruction),

    /// Error for an unmatched bracket.
    ///
    /// This should never actually be produced at runtime, as we validate that brackets match
    /// before running.
    UnmatchedBracket(Instruction),
}

impl BfVMError {
    /// Create a new BfVMError::IOError from a std::io::Error
    ///
    /// Manual wrapper to do so in order to introduce the instruction.
    fn convert_io_error(error: std::io::Error, instruction: Instruction) -> Self {
        Self::IOError(error, instruction)
    }
}

impl Display for BfVMError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BfVMError::EndOfTape(i) => write!(
                f,
                "{}:{}: {}",
                i.get_line(),
                i.get_column(),
                "End of Tape reached".to_string()
            ),
            BfVMError::IOError(e, i) => write!(f, "{}:{} {}", i.get_line(), i.get_column(), e),
            BfVMError::UnmatchedBracket(i) => write!(
                f,
                "{}:{} {}",
                i.get_line(),
                i.get_column(),
                "Unmatched bracket".to_string()
            ),
        }
    }
}

impl std::error::Error for BfVMError {}

/// Trait to allow a data type to be used as cells in the Brainfuck Virtual Machine
///
/// Ensures that cells must be able to handle wrapped increment and wrapped decrement.
pub trait CellKind: Debug + Default + Clone + PartialEq {
    /// Increment the value by one, wrapping if the max value is passed
    fn wrapped_increment(&mut self);

    /// Decrement the value by one, wrapping if the min value is passed
    fn wrapped_decrement(&mut self);

    /// Convert a u8 into an impl CellKind
    fn from_u8(&mut self, value: u8);

    /// Convert an impl CellKind to a u8
    fn to_u8(&self) -> u8;
}

impl CellKind for u8 {
    fn wrapped_increment(&mut self) {
        if *self == u8::max_value() {
            *self = u8::min_value();
        } else {
            *self += 1;
        }
    }

    fn wrapped_decrement(&mut self) {
        if *self == u8::min_value() {
            *self = u8::max_value();
        } else {
            *self -= 1;
        }
    }

    fn from_u8(&mut self, value: u8) {
        *self = value;
    }

    fn to_u8(&self) -> u8 {
        *self
    }
}

#[derive(Debug, PartialEq)]
/// A struct to represent the Virtual Machine brainfuck runs on
///
/// Brainfuck runs on a tape machine, which may or may not be fixed size. Classic Brainfuck has a
/// fixed sized tape of 30000 cells. There is also a head that points at a single point on the
/// tape.
///
/// Parametised by the type of the vector contents. This should be some form of int, and is a u8 in
/// classic Brainfuck.
///
/// ```
/// # use bft_types::BrainfuckProgram;
/// # use bft_interp::BrainfuckVM;
///
/// let mut bf_program = BrainfuckProgram::new("foo", "[]+-.,");
/// let bf_vm = BrainfuckVM::<u8>::new(30000, false, &mut bf_program);
/// ```
pub struct BrainfuckVM<'a, T> {
    tape: Vec<T>,
    head: usize,
    size: usize,
    can_grow: bool,
    program_counter: usize,
    program: &'a mut BrainfuckProgram,
}

impl<'a, T: CellKind> BrainfuckVM<'a, T> {
    /// Create a new BrainfuckVM of a given size
    ///
    /// Creates a new BrainfuckVM with size `size`. If `can_grow` is `true` then the tape will be
    /// allowed to be extended. If `size` is 0, then a default of 30000 is used.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::BrainfuckVM;
    ///
    /// let mut bf_program = BrainfuckProgram::new("bar", "[]--++.");
    /// let bf_vm = BrainfuckVM::<u8>::new(20, true, &mut bf_program);
    /// ```
    pub fn new(size: usize, can_grow: bool, program: &'a mut BrainfuckProgram) -> Self {
        let tape_size: usize = if size == 0 { 30000 } else { size };
        Self {
            tape: vec![T::default(); tape_size],
            head: 0,
            size: tape_size,
            can_grow,
            program_counter: 0,
            program,
        }
    }

    /// Moves pointer on tape left by one space
    ///
    /// Returns a BfVmError::EndOfTape if performing the move would go left of position 0. If
    /// successful, returns the position of the next instruction.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::{BfVMError, BrainfuckVM};
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[]+.,-<>");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// bf_vm.move_head_right();
    /// assert!(bf_vm.move_head_left().is_ok());
    /// ```
    pub fn move_head_left(&mut self) -> Result<usize, BfVMError> {
        if self.head == 0 {
            return Err(BfVMError::EndOfTape(
                *self.program.instruction_at(self.program_counter).unwrap(),
            ));
        }
        self.head -= 1;
        Ok(self.program_counter + 1)
    }

    /// Moves pointer on tape right by one space
    ///
    /// Returns a BfVmError::EndOfTape if performing the move would go right of the end of the
    /// tape, if the tape is non-extending. If successful, returns the position of the next
    /// instruction.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::{BfVMError, BrainfuckVM};
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[]+.,-<>");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// assert!(bf_vm.move_head_right().is_ok());
    /// ```
    pub fn move_head_right(&mut self) -> Result<usize, BfVMError> {
        if self.head + 1 == self.size && !self.can_grow {
            return Err(BfVMError::EndOfTape(
                *self.program.instruction_at(self.program_counter).unwrap(),
            ));
        }
        self.head += 1;
        Ok(self.program_counter + 1)
    }

    /// Increment the cell under the pointer by one. Wraps if maximum value is passed
    ///
    /// Returns the position of the next instruction, wrapped in a Result.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::{BfVMError, BrainfuckVM};
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[]+.,-<>");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// bf_vm.increment();
    /// ```
    pub fn increment(&mut self) -> Result<usize, BfVMError> {
        T::wrapped_increment(self.current_cell_mut());
        Ok(self.program_counter + 1)
    }

    /// Decrement the cell under the pointer by one. Wraps if maximum value is passed
    ///
    /// Returns the position of the next instruction, wrapped in a Result.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::{BfVMError, BrainfuckVM};
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[]+.,-<>");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// bf_vm.decrement();
    /// ```
    pub fn decrement(&mut self) -> Result<usize, BfVMError> {
        T::wrapped_decrement(self.current_cell_mut());
        Ok(self.program_counter + 1)
    }

    /// Read a byte of input and store it at the current pointer position
    ///
    /// The input is anything that implements std::io::Read.
    ///
    /// Returns the position of the next instruction, or a BfVMError, wrapping around a
    /// std::io::Error.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::BrainfuckVM;
    /// let mut input = std::io::Cursor::new(vec!(1_u8));
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", ".[]++");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// assert!(bf_vm.input(&mut input).is_ok());
    /// ```
    pub fn input(&mut self, input: &mut impl Read) -> Result<usize, BfVMError> {
        let mut buf = [0; 1];
        match input.read(&mut buf[..]) {
            Ok(_) => self.current_cell_mut().from_u8(buf[0]),
            Err(e) => {
                return Err(BfVMError::convert_io_error(
                    e,
                    *self.program.instruction_at(self.program_counter).unwrap(),
                ))
            }
        };
        Ok(self.program_counter + 1)
    }

    /// Write the value at the pointer to a writer.
    ///
    /// The output is anything that implements std::io::Write.
    ///
    /// Returns the position of the next instruction, or a BfVMError, wrapping around a
    /// std::io::Error.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::BrainfuckVM;
    /// let mut output = std::io::Cursor::new(vec!(1_u8));
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", ".[]++");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// assert!(bf_vm.output(&mut output).is_ok());
    /// ```
    pub fn output(&mut self, output: &mut impl Write) -> Result<usize, BfVMError> {
        let buf = [self.current_cell().to_u8()];
        match output.write(&buf[..]) {
            Ok(_) => Ok(self.program_counter + 1),
            Err(e) => Err(BfVMError::convert_io_error(
                e,
                *self.program.instruction_at(self.program_counter).unwrap(),
            )),
        }
    }

    /// The start of a looping structure
    ///
    /// Checks whether the current cell is zero. If not, then moves to the next instruction.
    /// However, if the current cell is zero, we move to the instruction after the matching close
    /// bracket.
    ///
    /// The below example is a little contrived.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::BrainfuckVM;
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[+.]");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    /// assert_eq!(bf_vm.begin_loop().unwrap(), 4);
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[+.]");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    /// bf_vm.increment();
    /// assert_eq!(bf_vm.begin_loop().unwrap(), 1);
    /// ```
    pub fn begin_loop(&mut self) -> Result<usize, BfVMError> {
        if *self.current_cell() != T::default() {
            return Ok(self.program_counter + 1);
        }
        match self.program.match_bracket(self.program_counter) {
            Ok(n) => Ok(n + 1),
            Err(_) => Err(BfVMError::UnmatchedBracket(
                *self.program.instruction_at(self.program_counter).unwrap(),
            )),
        }
    }

    /// The end of a looping structure
    ///
    /// Checks whether the current cell is zero. If it is, then moves to the next instruction.
    /// However, if the current cell is nonzero, we move to the instruction after the matching open
    /// bracket.
    ///
    /// The below example is not exactly illustrative, as there is no API to move the program
    /// counter to an arbitrary position as of yet.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::BrainfuckVM;
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "[+.]");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// bf_vm.end_loop();
    /// ```
    pub fn end_loop(&mut self) -> Result<usize, BfVMError> {
        if *self.current_cell() == T::default() {
            return Ok(self.program_counter + 1);
        }
        match self.program.match_bracket(self.program_counter) {
            Ok(n) => Ok(n + 1),
            Err(_) => Err(BfVMError::UnmatchedBracket(
                *self.program.instruction_at(self.program_counter).unwrap(),
            )),
        }
    }

    /// Interprets the loaded program.
    ///
    /// This actually runs the loaded BrainfuckProgram, interpreting the Instructions as the
    /// corresponding functions on the VM.
    ///
    /// ```
    /// # use bft_types::BrainfuckProgram;
    /// # use bft_interp::BrainfuckVM;
    /// let mut input = std::io::Cursor::new(Vec::new());
    /// let mut output = std::io::Cursor::new(Vec::new());
    ///
    /// let mut bf_program = BrainfuckProgram::new("foo", "+><-");
    /// let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
    ///
    /// assert!(bf_vm.interpret(&mut input, &mut output).is_ok());
    /// ```
    pub fn interpret(
        &mut self,
        input: &mut impl Read,
        output: &mut impl Write,
    ) -> Result<(), BfVMError> {
        while self.program_counter < self.program.len() {
            self.program_counter = match self
                .program
                .instruction_at(self.program_counter)
                .unwrap()
                .command()
            {
                RawInstruction::MoveLeft => self.move_head_left()?,
                RawInstruction::MoveRight => self.move_head_right()?,
                RawInstruction::Increment => self.increment()?,
                RawInstruction::Decrement => self.decrement()?,
                RawInstruction::Input => self.input(input)?,
                RawInstruction::Output => self.output(output)?,
                RawInstruction::BeginLoop => self.begin_loop()?,
                RawInstruction::EndLoop => self.end_loop()?,
            };
        }
        Ok(())
    }

    /// Safely retrieve the cell currently pointed at in the VM
    ///
    /// Call to unwrap guaranteed to succeed by the assert above.
    ///
    /// In future, this assert should raise an error rather than panic!ing
    fn current_cell(&self) -> &T {
        assert!(self.head < self.size);
        self.tape.get(self.head).unwrap()
    }

    /// Safely retrieve the cell currently pointed at in the VM as a mutable reference
    ///
    /// Call to unwrap guaranteed to succeed by the assert above.
    ///
    /// In future, this assert should raise an error rather than panic!ing
    fn current_cell_mut(&mut self) -> &mut T {
        assert!(self.head < self.size);
        self.tape.get_mut(self.head).unwrap()
    }
}

#[cfg(test)]
mod bft_interp_tests {
    use super::*;
    use bft_types::BrainfuckProgram;

    #[test]
    fn test_bfvm_head_left() {
        let mut bf_program = BrainfuckProgram::new("foo", "[]++");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.move_head_right().is_ok());
        assert!(bf_vm.move_head_left().is_ok());
        assert!(bf_vm.move_head_left().is_err());
    }

    #[test]
    fn test_bfvm_head_right() {
        let mut bf_program = BrainfuckProgram::new("foo", "[]++");
        let mut bf_vm = BrainfuckVM::<u8>::new(2, false, &mut bf_program);

        assert!(bf_vm.move_head_right().is_ok());
        assert!(bf_vm.move_head_right().is_err());
    }

    #[test]
    fn test_u8_wrapped() {
        let x: &mut u8 = &mut 0;
        &u8::wrapped_increment(x);
        assert_eq!(*x, 1_u8);
        &u8::wrapped_decrement(x);
        assert_eq!(*x, 0_u8);
        &u8::wrapped_decrement(x);
        assert_eq!(*x, u8::max_value());
        &u8::wrapped_increment(x);
        assert_eq!(*x, u8::min_value());
    }

    #[test]
    fn test_bfvm_increment() {
        use bft_types::BrainfuckProgram;

        let mut bf_program = BrainfuckProgram::new("foo", "[]+.,-<>");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.increment().is_ok());

        assert_eq!(*bf_vm.current_cell(), 1_u8);
    }

    #[test]
    fn test_bfvm_decrement() {
        use bft_types::BrainfuckProgram;

        let mut bf_program = BrainfuckProgram::new("foo", "[]+.,-<>");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.decrement().is_ok());

        assert_eq!(*bf_vm.current_cell(), u8::max_value());
    }

    #[test]
    fn test_bfvm_input() {
        let mut input = std::io::Cursor::new(vec![1_u8]);

        let mut bf_program = BrainfuckProgram::new("foo", ".[]++");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.input(&mut input).is_ok());
        assert_eq!(*bf_vm.current_cell(), 1_u8);
    }

    #[test]
    fn test_bfvm_output() {
        let mut output = std::io::Cursor::new(vec![1_u8]);

        let mut bf_program = BrainfuckProgram::new("foo", "+-,,[]");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.output(&mut output).is_ok());
        assert_eq!(output.get_ref()[0], 0_u8);
    }

    #[test]
    fn test_bfvm_begin_loop() {
        let mut bf_program = BrainfuckProgram::new("foo", "[+-,,[]]");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert_eq!(bf_vm.begin_loop().unwrap(), 8);
    }

    #[test]
    fn test_bfvm_end_loop() {
        let mut bf_program = BrainfuckProgram::new("foo", "[+-,,[]]");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);
        bf_vm.program_counter = 6;

        assert_eq!(bf_vm.end_loop().unwrap(), 7);
        assert!(bf_vm.increment().is_ok());
        bf_vm.program_counter = 7;
        assert_eq!(bf_vm.end_loop().unwrap(), 1);
    }

    #[test]
    fn test_bfvm_interpret() {
        let mut input = std::io::Cursor::new(vec![1_u8]);
        let mut output = std::io::Cursor::new(vec![0_u8]);

        let mut bf_program = BrainfuckProgram::new("foo", ">>+<->,.");
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.interpret(&mut input, &mut output).is_ok());
        assert_eq!(output.get_ref()[0], 1_u8);
    }

    #[test]
    fn test_bfvm_hello_world_run() {
        let mut input = std::io::Cursor::new(Vec::new());
        let mut output = std::io::Cursor::new(Vec::new());

        let mut bf_program = BrainfuckProgram::new(
            "foo",
            "+[-[<<[+[--->]-[<<<]]]>>>-]>-.---.>..>.<<<<-.<+.>>>>>.>.<<.<-.",
        );
        let mut bf_vm = BrainfuckVM::<u8>::new(0, false, &mut bf_program);

        assert!(bf_vm.interpret(&mut input, &mut output).is_ok());
    }
}
